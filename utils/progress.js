exports = module.exports = ProgressBar;
function ProgressBar(fmt, options) {
  this.stream = options.stream || process.stderr;

  if (typeof(options) === 'number') {
    let total = options;
    options = {};
    options.total = total;
  }
  else {
    options = options || {};
    if (typeof fmt !== 'string') {
      throw new Error('format required');
    }
    if (typeof options.total !== 'number') {
      throw new Error('total required');
    }
  }

  this.fmt = fmt;
  this.curr = options.curr || 0;
  this.total = options.total;
  this.width = options.width || this.total;
  this.clear = options.clear;
  this.chars = {
    complete   : options.complete || '=',
    incomplete : options.incomplete || '-',
    head       : options.head || (options.complete || '=')
  };
  this.renderThrottle = options.renderThrottle !== 0 ? (options.renderThrottle || 16) : 0;
  this.callback = options.callback || function () {};
  this.tokens = {};
  this.lastDraw = '';
}


ProgressBar.prototype.tick = function (len, tokens) {
  if (len !== 0) {
    len = len || 1;
  }


  if (typeof len === 'object') {
    tokens = len, len = 1;
  }
  if (tokens) {
    this.tokens = tokens;
  }


  if (this.curr === 0) {
    this.start = new Date;
  }

  this.curr += len;

  if (!this.renderThrottleTimeout) {
    this.renderThrottleTimeout = setTimeout(this.render.bind(this), this.renderThrottle);
  }

  if (this.curr >= this.total) {
    if (this.renderThrottleTimeout) {
      this.render();
    }
    this.complete = true;
    this.terminate();
    this.callback(this);
  }
};

ProgressBar.prototype.render = function (tokens) {
  clearTimeout(this.renderThrottleTimeout);
  this.renderThrottleTimeout = null;

  if (tokens) {
    this.tokens = tokens;
  }

  if (!this.stream.isTTY) {
    return;
  }

  let ratio = this.curr / this.total;
  ratio = Math.min(Math.max(ratio, 0), 1);

  let percent = ratio * 100;
  let incomplete, complete, completeLength;
  let elapsed = new Date - this.start;
  let eta = (percent === 100) ? 0 : elapsed * (this.total / this.curr - 1);
  let rate = this.curr / (elapsed / 1000);

  let str = this.fmt.
    replace(':current', this.curr).
    replace(':total', this.total).
    replace(':elapsed', isNaN(elapsed) ? '0.0' : (elapsed / 1000).toFixed(1)).
    replace(':eta', (isNaN(eta) || !isFinite(eta)) ? '0.0' : (eta / 1000).toFixed(1)).
    replace(':percent', `${percent.toFixed(0)}%`).
    replace(':rate', Math.round(rate));

  let availableSpace = Math.max(0, this.stream.columns - str.replace(':bar', '').length);
  if (availableSpace && process.platform === 'win32'){
    availableSpace = availableSpace - 1;
  }

  let width = Math.min(this.width, availableSpace);

  completeLength = Math.round(width * ratio);
  complete = Array(Math.max(0, completeLength + 1)).join(this.chars.complete);
  incomplete = Array(Math.max(0, width - completeLength + 1)).join(this.chars.incomplete);

  if (completeLength > 0) {
    complete = complete.slice(0, -1) + this.chars.head;
  }

  str = str.replace(':bar', complete + incomplete);

  if (this.tokens) {
    for (let key in this.tokens) {
      str = str.replace(`:${key}`, this.tokens[key]);
    }
  }

  if (this.lastDraw !== str) {
    this.lastDraw = str;
  }
};

ProgressBar.prototype.update = function (ratio, tokens) {
  let goal = Math.floor(ratio * this.total);
  let delta = goal - this.curr;

  this.tick(delta, tokens);
};


ProgressBar.prototype.interrupt = function (message) {
  this.stream.clearLine();
  this.stream.cursorTo(0);
  this.stream.write(message);
  this.stream.write('\n');
  this.stream.write(this.lastDraw);
};

ProgressBar.prototype.terminate = function () {
  if (this.clear) {
    if (this.stream.clearLine) {
      this.stream.clearLine();
      this.stream.cursorTo(0);
    }
  }
  else {
  }
};
